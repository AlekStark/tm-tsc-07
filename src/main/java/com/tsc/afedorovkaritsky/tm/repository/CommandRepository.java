package com.tsc.afedorovkaritsky.tm.repository;

import com.tsc.afedorovkaritsky.tm.constant.ArgumentConst;
import com.tsc.afedorovkaritsky.tm.constant.TerminalConst;
import com.tsc.afedorovkaritsky.tm.model.Command;

public class CommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "О разработчике"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "О системе"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Помощь"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Версия"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Выход"
    );

    public static final Command[] COMMAND = new Command[] {
            ABOUT, INFO, VERSION, HELP, EXIT
    };

    public static Command[] getCommand() {
        return COMMAND;
    }
}
